package edu.towson.cosc435.labsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import edu.towson.cosc435.labsapp.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // TODO - Build the user interface (EditText, Spinner, EditText, Button, TextView)
        // TODO - Add a values file that defines an array for the operators
        // TODO - Add a button click listener and compute the result
        // TODO - Extra: Add multi-language support to the result string
    }
}
